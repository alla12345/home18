//1---------------
function Calc() {
  this.count = 0;

  this.sum = (number) => {
    this.count = this.count + number;
    return this.count;
  }

  this.sub = (number) => {
    this.count = this.count - number;
    return this.count;
  }  
}

const calcOne = new Calc(); 
calcOne.sum(3);
calcOne.sub(5);
console.log(calcOne.count);

const calcTwo = new Calc(); 
calcTwo.sum(3);
console.log(calcTwo.count);

//2-------------  
class NewCalc {
  #count;
  constructor(count) {
    this.#count = count;
  }

  sum = (number) => {
    this.count = this.count + number;
  }

  sub = (number) => {
    this.count = this.count - number;
  } 
  
  get count() {
    return this.#count;
  }

  set count(value) {
    if (this.#count > 10) {
      console.log('слишком большое значение');
      return this.#count; 
    }
    this.#count = value; 
  }
} 

const calcO = new NewCalc(0); 
calcO.sum(3);
calcO.sub(5);
console.log(calcO.count);

const calcT = new NewCalc(5); 
calcT.sum(3);
console.log(calcT.count);

//3------------------
class Person {
  constructor(age, oneEat) {
    this.age = age;
    this.oneEat = oneEat;
  }

  nowEat = 0;
  maxEat = 100;

  eat = () => {
    if ((this.nowEat + this.oneEat) < this.maxEat) {
      this.nowEat = this.nowEat + this.oneEat;
    } else {
      console.log('сыт');
    }
  };
}

class Man extends Person {
  constructor (age, oneEat, strong) {
    super(age, oneEat);
    this.strong = strong;
  }
}

class Woman extends Person {
  constructor (age, oneEat, size) {
    super(age, oneEat);
    this.size = size;
  }
} 

const manOne = new Man(20, 51, 7);
const manTwo = new Man(5, 2, 2);

const womanOne = new Man(20, 3, 1);
const womanTwo = new Man(5, 2, 0);

manOne.eat();
manOne.eat();

console.log(manOne.nowEat);
console.log(manTwo.nowEat);
